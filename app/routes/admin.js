module.exports = function(app) {
    app.get('/nova_noticia', function(requisicao, resposta) {
        resposta.render('admin/form_add_noticia');
    });

    app.post('/noticias/salvar', function(requisicao, resposta) {
        const noticia = requisicao.body;

        const conexao = app.config.dbConnection();
        const incluirNoticia = new app.app.models.NoticiasModel(conexao);

        incluirNoticia.newNoticia(noticia, function(erro, resultado) {
            resposta.redirect('/noticias');
        });
    });
};
