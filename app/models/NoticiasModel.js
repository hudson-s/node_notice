class Noticias {
    constructor(conexao) {
        this.conexao = conexao;
    }

    getNoticia(fCallback) {
        this.conexao.query('select * from noticias where id_noticia = 15',
            fCallback);
    }

    getNoticias(fCallback) {
        this.conexao.query('select * from noticias', fCallback);
    }

    newNoticia(noticia, fCallback) {
        this.conexao.query('insert into noticias set ?', noticia, fCallback);
    }
}


module.exports = ()=> Noticias;

/* function Noticias(conexao) {
    this._conexao = conexao;
}

Noticias.prototype.getNoticia = function(fCallback) {
    this._conexao.query('select * from noticias where id_noticia = 15',
        fCallback);
}

Noticias.prototype.getNoticias = function(fCallback) {
    this._conexao.query('select * from noticias', fCallback);
}

Noticias.prototype.newNoticia = function(noticia, fCallback) {
    this._conexao.query('insert into noticias set ?', noticia, fCallback);
} */
